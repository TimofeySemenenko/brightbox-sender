# KIA взаимодействия с RabbitMq и отправка данных в Brightbox

Задача: https://redmine.idefa.ru/issues/4150

## Слушает очереди в RabbitMQ:
- rabbitMqReader

## Обрабатывает очереди:
- taskWorker

## Обрабатывает shell команды:
- shellOut

## Выполняет функции:
- Подключение к RabbitMQ
- Получение канала
- Определение очереди
- Чтение очериди из канала
- Обработки задачи (выполнение shell команды)
- Обработка ошибок
- Запись в логи

## Параметры, передаваемые в приложение brightbox-sender (формат json)
- QueueNames: название очереди
- RabbitMqConnectStrings: реквизиты RabbitMq
- LogFiles: путь до файла с логами
- GetPhpScripts: путь до php скрипта, который запускает php приложение

## Конфигурация backend test
```json
{
  "queueName": "BRIGHTBOX",
  "rabbitMqConnect": "amqp://guest:guest@192.168.11.5:5672",
  "logFiles": "/Users/viktor.koloty/repos/go/projects/brightbox-sender/brightbox-sender.log",
  "getPhpScript": "/home/kiadev/dev-trofimov/html/_scripts/from_rabbit/send_to_brightbox.php"
}
```

## Конфигурация backend prod
```json
{
TODO: дополнить!
}
```