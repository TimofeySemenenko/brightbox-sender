package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/streadway/amqp"
	"io/ioutil"
	log "github.com/defa-public/logrus"
	"os"
	"os/exec"
	"io"
	"bytes"
)

// конфиг приложения описан через json структуру
type configuration struct {
	QueueNames             string `json:"queueName"`
	RabbitMqConnectStrings string `json:"rabbitMqConnect"`
	LogFiles               string `json:"logFiles"`
	GetPhpScripts          string `json:"getPhpScript"`
}

// константы
var (
	config     configuration
	rabbitConn *amqp.Connection
	rabbitChan *amqp.Channel
)

// иницивлизация конфига
func init() {
	// Load config
	configPath := flag.String("config", "", "")
	flag.Parse()
	config = loadConfig(*configPath)

	// Init log
	logFile, err := os.OpenFile(config.LogFiles, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	panicOnError("Fail create log file", err)
	log.SetFormatter(&log.TextFormatter{})
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)
	log.SetLevel(log.DebugLevel)
}

// вызов основной go рутины
func main() {
	rabbitConn = rabbitMqConnection()
	defer rabbitConn.Close()
	rabbitChan = rabbitMqChannel(rabbitConn)
	defer rabbitChan.Close()
	q := rabbitMqQueue(rabbitChan, config.QueueNames)
	rabbitMqReader(rabbitChan, q)
}

// Загружаем файл конфига
func loadConfig(path string) configuration {
	file, err := ioutil.ReadFile(path)
	panicOnError("Fail read config", err)
	err = json.Unmarshal(file, &config)
	panicOnError("Fail parse config", err)
	return config
}

// создаём подключение к Rabbitmq
func rabbitMqConnection() (*amqp.Connection) {
	log.Infof("Create RabbitMq connection")
	conn, err := amqp.Dial(config.RabbitMqConnectStrings)
	panicOnError("Fail to connect to RabbitMQ", err)
	log.Infof("Success RabbitMq connection")
	return conn
}

// получаем канал
func rabbitMqChannel(conn *amqp.Connection) (*amqp.Channel) {
	log.Infof("Open RabbitMq channel")
	ch, err := conn.Channel()
	panicOnError("Fail to open a channel", err)
	log.Infof("Success open RabbitMq channel")
	return ch
}

// определяем очередь
func rabbitMqQueue(ch *amqp.Channel, rabbitQueueName string) (amqp.Queue) {
	log.Infof("Open RabbitMq queue")
	q, err := ch.QueueDeclare(
		rabbitQueueName,
		true,
		false,
		false,
		false,
		nil,
	)
	panicOnError("Fail to declare a queue", err)
	return q
}

// чтение очериди из канала
func rabbitMqReader(ch *amqp.Channel, q amqp.Queue) {
	delivery, err := ch.Consume(
		q.Name, // queue
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	panicOnError("Fail register a consumer RabbitMQ", err)
	forever := make(chan bool)
	go taskWorker(delivery)
	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

// процедуда обработки задачи
func taskWorker(tasks <-chan amqp.Delivery) {
	for taskItem := range tasks {
		go taskWorkerItem(taskItem)
	}
}

// процедура выполнения команды
func taskWorkerItem(taskItem amqp.Delivery) {
	fmt.Print(taskItem)
	defer skipQueueErrorToLog()
	dataTask := "'" + string(taskItem.Body) + "'"
	fmt.Println(dataTask)
	log.Info("Start command do")
	err := shellOut("php " + config.GetPhpScripts + " " + dataTask)
	panicOnError("Command finished with error", err)
	log.Info("Finished success")

}

// функция для выполнение shell комманд
func shellOut(cmd string) (err error) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	log.Infof("Execute command: " + cmd)
	command := exec.Command("sh", "-c", cmd)
	err = command.Start()
	if err != nil {
		return fmt.Errorf("failed to execute %v, err: %v", cmd, err)
	}
	err = command.Wait()
	if err != nil {
		return fmt.Errorf("command %v stdout: %q, stderr: %q, finished with error: %v", cmd, stdout.String(), stderr.String(), err)
	}
	return nil
}

// обработка паники
func panicOnError(msg string, err error) {
	if err != nil {
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func skipQueueErrorToLog() {
	err := recover()
	if err != nil {
		log.Info("Save panic")
		log.Debug(err)
	}
}
